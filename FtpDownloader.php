<?php

include_once 'vendor/autoload.php';

Class FtpHelpers
{
    private static $_extensions_to_remove = ['.JPG','.JPEG'];

    public static function dateInRange($userDate, $checkDate)
    {
        $start_ts = strtotime($checkDate) - getenv('interval');
        $end_ts   = strtotime($checkDate) + getenv('interval');
        $user_ts  = strtotime($userDate);

        $isContained = (($user_ts >= $start_ts) && ($user_ts <= $end_ts));

        return $isContained;
    }

    public static function getPhotoTimeStamp($photo, $folder)
    {
        $output = strtoupper($photo);
        $output = str_replace($folder,'',$output);

        foreach (self::$_extensions_to_remove as $extension)
            $output = str_replace($extension,'',$output);

        return $output;
    }

    public static function getPhotoRange($timestamp, $ranges)
    {
        if (!is_array($ranges))             return $timestamp;
        if (empty($ranges))                 return $timestamp;
        if (in_array($timestamp, $ranges))  return $timestamp;

        foreach (array_keys($ranges) as $photoTimestamp)
        {
            if (FtpHelpers::dateInRange($timestamp, $photoTimestamp)) return $photoTimestamp;
        }

        return $timestamp;
    }


    public static function rrmdir($dir) {

        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object))
                        self::rrmdir($dir. DIRECTORY_SEPARATOR .$object);
                    else
                        unlink($dir. DIRECTORY_SEPARATOR .$object);
                }
            }
            rmdir($dir);
        }
    }

}

class FtpDownloader
{
    const dealersFile = 'dealers.csv';

    /**
     * @var SftpConnector
     */
    private $_conn;
    private $_root;
    private $_mappedFolders = array();
    private $_dayDirList    = array();
    private $_cameraDirList = array();

    private $_paths  = array();
    private $_ranges = array();

    private $_lastError;

    public function __construct($ip, $port, $user, $pass)
    {
        $this->_root = getenv('ftproot');
        $this->_conn = new SftpConnector($ip, $port, $user, $pass);
    }

    public function getLastError()
    {
        return $this->_lastError;
    }

    /***
     * Returns the list of folder agreed with the customer: folder must exist under dealers.csv file
     * If a CUT is specified, only DVR matching CUT is added to mappedFolder to scan
     * @param bool $cut
     * @return array
     */
    public function getMappedFolders($cut = FALSE):array
    {
        $a = array();
        $file = fopen(self::dealersFile, 'r');
        while (($line = fgetcsv($file)) !== FALSE) {
            $dvr = $line[0];
            $kn  = $line[1];
            if (($cut === FALSE) or ($cut == $dvr)) {
                $a[$dvr] = $kn;
            }
        }
        fclose($file);

        $this->_mappedFolders = $a;
        return $a;
    }

    public function scanFolder()
    {

    }

    public function scanMappedFolders($mappedFolders)
    {
        foreach ($mappedFolders as $dvr => $kundenNummer)
        {
            $currentDvrDir = $this->_root . FTP_DIRECTORY_SEPARATOR . $dvr;
            $this->_dayDirList = $this->_conn->listFolderContent($currentDvrDir);
        }
    }

    public function scanDaysFolders($dayDirList)
    {
        foreach ($dayDirList as $day) {
            $currentDayDir = $currentDvrDir . FTP_DIRECTORY_SEPARATOR . $day;
            $this->_cameraDirList = $this->_conn->listFolderContent($currentDayDir);
        }
    }

    public function getPaths()
    {
        return $this->_paths;
    }

    public function getRanges()
    {
        return $this->_ranges;
    }

    /***
     * Scans FTP site following a predetermined algorithm and returns a list of files as an array that has to be
     * retrieved lately; return TRUE if everything's OK, FALSE otherwise (see exceptions thrown in this case
     * @param bool $closeConnection TRUE by default, closes connection at the end of FTP SCAN
     * @return boolean
     */
    public function scanFtpSite()
    {
        $mappedFolders = $this->_mappedFolders;
        if (empty($mappedFolders)) return FALSE;

        //$this->_dayDirList    = $this->scanMappedFolders($mappedFolders);

        //$this->_cameraDirList = $this->scanDaysFolders($this->_dayDirList);

        try {
            foreach ($mappedFolders as $dvr => $kundenNummer) {
                $currentDvrDir = $this->_root . FTP_DIRECTORY_SEPARATOR . $dvr;
                $dayDirList = $this->_conn->listFolderContent($currentDvrDir);

                foreach ($dayDirList as $day) {
                    $currentDayDir = $currentDvrDir . FTP_DIRECTORY_SEPARATOR . $day;
                    $cameraDirList = $this->_conn->listFolderContent($currentDayDir);

                    foreach ($cameraDirList as $camera) {
                        $currentCameraDir = $currentDayDir . FTP_DIRECTORY_SEPARATOR . $camera;
                        $photoList = $this->_conn->listFolderContent($currentCameraDir);

                        foreach ($photoList as $photo) {
                            $path = $currentCameraDir . FTP_DIRECTORY_SEPARATOR . $photo;
                            $photoTimeStamp = FtpHelpers::getPhotoTimeStamp($photo, $camera);
                            $range = @FtpHelpers::getPhotoRange($photoTimeStamp, $this->_ranges[$kundenNummer]);
                            $this->_ranges[$kundenNummer][$range][] = $path;
                            $this->_paths[] = $path;
                        }
                    }
                }
            }
        } catch (Exception $e)
        {
            $this->_lastError = $e;
            $this->_conn->curlClose();
            return FALSE;
        }

        return TRUE;
    }

    public function downloadScannedTree($closeConnection = TRUE)
    {
        $localRepo = getenv('localrepo');
        @mkdir($localRepo);

        if (empty($this->_ranges))
        {
            $this->_lastError = "FTP dir is empty or you have not performed directory scan first (scanFtpSite)";
            return FALSE;
        }

        try
        {
            foreach ($this->_ranges as $kn => $timestamps) {
                $dvrRepo = $localRepo . DIRECTORY_SEPARATOR . $kn;
                @FtpHelpers::rrmdir($dvrRepo);
                @mkdir($dvrRepo);

                foreach ($timestamps as $timestamp => $paths) {
                    $rangeDir = $localRepo . DIRECTORY_SEPARATOR . $kn . DIRECTORY_SEPARATOR . $timestamp;
                    @mkdir($rangeDir);

                    foreach ($paths as $remotePath) {
                        $localPath = $rangeDir . DIRECTORY_SEPARATOR . basename($remotePath);
                        if ($this->_conn->downloadFile($remotePath, $localPath) === FALSE) continue;   //if the file is not downloaded, remote delete is skipped out
                        $this->_conn->deleteFile($remotePath);
                        $this->_conn->deleteDir(dirname($remotePath));             //tries to delete parent CAMERA folder, if empty
                        $this->_conn->deleteDir(dirname(dirname($remotePath)));    //tries to delete parent DAY folder, if empty
                    }
                }
            }
        } catch (Exception $e)
        {
            $this->_lastError = $e;
            return FALSE;
        } finally {
            if ($closeConnection) $this->_conn->curlClose();
        }

        return TRUE;
    }

    public function showCurrentStats()
    {

    }

}