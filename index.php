<?php
include_once 'engine/AXA_export.php';
include_once 'engine/ObjToXml.php';
include_once 'engine/ftp.php';

define ('REMOTE_DIR', 'mbcw_plugin/TO_DAT');
define ('DIR_TOPROCESS', 'store/to_process');
define ('$file_to_retrieve', 'store/processed');
define ('DIR_OUTPUT', '/mb_charterway/FROM_DAT');

define ('FTP_IP', '195.30.103.99');
define ('FTP_PORT', '2222');
define ('FTP_USER', 'mb_charterway');
define ('FTP_PASS', 'dohNgieg2v');

function uploadToSftp($fileName)
{
	$ip   = FTP_IP;
	$port = FTP_PORT;
	$user = FTP_USER;
	$pass = FTP_PASS;
	
	try
	{
		echo 'connecting to ftp... ';
		$ftpSite = new SftpConnector($ip, $port, $user, $pass);
		/*$ftpSite->connect();
		die;
		echo 'DONE</br>';*/

		echo 'uploading files...</br>';
		$ftpSite->upload2(DIR_OUTPUT, $fileName);
		//$ftpSite->close();
		echo 'DONE</br>';
		return true;
	}
	catch (Exception $e)
	{
		echo 'connection failed or upload failed';
		return false;
	}
}

function createCsvFile($name, $body)
{
	$fp = fopen("./output/$name.csv", 'w');
	fwrite($fp, $body);
	fclose($fp);
}

function main()
{
	$settings = new Settings();

	//$dbc = new DbConnector();
	//$dbc->connectPDO();
	$urlParams = new UrlParameters($_GET);


	/*$projectId = $urlParams->getProjectId();
	$projectId = 1;*/

	if (isset($_GET["dossierId"]))
	{
		$dossierToProcess = $_GET["dossierId"];
		$settings->setSettingsProvider($urlParams);
	}
	else
	{
		die('no dossier provided');
	}

	if (isset($_GET["type"]))
	{
		$type = $_GET["type"];
	}
	else
	{
		die('no type provided');
	}
		
	$dossierId = $settings->getDossierId();
	echo "PROCESSING DOSSIER $dossierId</br><hr>";

	
	$getContract = new GetContract($settings);
	$dossier = $getContract->perform();
	
	switch ($type)
	{	
		case 'authority':
			$body ="{$dossier->getTemplateData('numeroManutenzione')};30;{$dossier->getTemplateData('operatoreOCS')};{$dossier->getTemplateData('OperatoreDAT')};{$dossier->getTemplateData('LicenseNumber')};{$dossier->getTemplateData('tipoAutorizzazione')};{$dossier->getTemplateData('tipoIntervento')};{$dossier->getTemplateData('codiceIntervento')};{$dossier->gettotaleGomme()};{$dossier->getTemplateData('tipoGomme')};{$dossier->getTemplateData('dataRicovero')};{$dossier->getTemplateData('dataPrevistaRestituzione2')};{$dossier->getTemplateData('numeroSinistro2')};{$dossier->getTemplateData('mileageOdometer')};{$dossier->gettotaleimponibile()};{$dossier->getTemplateData('descrizioneDanni')}";

			//Man_<yyyyMMdd_HHmmss>.csv
			$date = date('Ymd_His', time());
			$name = "Man_$date";
			createCsvFile($name, $body);
			$fileName = "$name.csv";
			if (uploadToSftp($fileName))
				rename("./output/$fileName", "./output/processed/$fileName");
			
			break;
		case 'fattura':	
			$body = "{$dossier->getTemplateData('numeroAutorizzazione')};{$dossier->getTemplateData('numeroSinistro2')}";
			
			$date = date('Ymd_His', time());
			$name = "Fatture_$date";
			createCsvFile($name, $body);
			$fileName = "$name.csv";
			if (uploadToSftp($fileName))
				rename("./output/$fileName", "./output/processed/$fileName");
			break;
		case 'auto':	
			$body ="{$dossier->getTemplateData('numeroManutenzione')};10;{$dossier->getTemplateData('operatoreOCS')};AU;{$dossier->getTemplateData('LicenseNumber')};{$dossier->getTemplateData('tipoAutorizzazione')};{$dossier->getTemplateData('tipoIntervento')};{$dossier->getTemplateData('codiceIntervento')};{$dossier->gettotaleGomme()};{$dossier->getTemplateData('tipoGomme')};{$dossier->getTemplateData('dataRicovero')};{$dossier->getTemplateData('dataPrevistaRestituzione2')};{$dossier->getTemplateData('numeroSinistro2')};{$dossier->getTemplateData('mileageOdometer')};{$dossier->gettotaleimponibile()};{$dossier->getTemplateData('descrizioneDanni')}";
			
			//Man_<yyyyMMdd_HHmmss>.csv
			$date = date('Ymd_His', time());
			$name = "Man_$date";
			createCsvFile($name, $body);
			$fileName = "$name.csv";
			if (uploadToSftp($fileName))
				rename("./output/$fileName", "./output/processed/$fileName");
			break;
		default:
			die('type not correct');
	}

	echo 'END';
}

main();