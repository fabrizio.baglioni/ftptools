<?php
define('URL_ENVIRONMENT', 'env');
define('DEFAULT_ENVIRONMENT', 'PROD');

putenv("ENVIRONMENT=".(!isset($_GET[URL_ENVIRONMENT])?DEFAULT_ENVIRONMENT:strtoupper($_GET[URL_ENVIRONMENT])));
$config = include ('config.php');

if (!array_key_exists(getenv('ENVIRONMENT'),$config))
{
	die("[".getenv('ENVIRONMENT')."]Environment parameters undefined");
}
else
{
	foreach ($config[getenv('ENVIRONMENT')] as $var => $val)
	{
		putenv("$var=$val");
	}
}