<?php
define ('URL_ENVIRONMENT', 'env');
define ('URL_PROJECTID', 'prjid');
define ('URL_DOSSIERID', 'dossierId');

define ('CUSTOMER_LOGIN', 'ferrsimo');
define ('CUSTOMER_NUMBER', '3133320');
define ('CUSTOMER_SIGNATURE', 'akEwRUF3TUNZYmR4V0VTK2lxOWd5U3JDN0I3a2pIQWJMN2t4RUx4ZmRDTEdWUHcyYUhueWdubnc0dHFRT1J4bDg2R2VpblB3NERZbHNQdz0=');
define ('INTERFACE_PARTNER_NUMBER', '3131411');
define ('INTERFACE_PARTNER_SIGNATURE', 'jA0EAwMCrjjoCCEuBBBgySvQtBjnp+NKnkteqoMkkzMRZlXddAQsnEG1vdDRAyRfRq+a41mrUtxJrmOg');

define ('DB_HOSTNAME', 'localhost');
define ('DB_DBNAME', 'axa_it_stats');
define ('DB_USERNAME', 'axa_it_stats');
define ('DB_PASSWORD', 'zV7UB16dzlCDJN59');

interface SettingsProvider
{
	public function getEnvironment();
	public function getProjectId();
	public function getDossierId();
}

Class ParametersFromDb implements SettingsProvider
{	
	private $_projectId = -1;
	private $_dossierId = -1;
	
	public function __construct($projectId)
	{
		$this->_projectId = $projectId;
		
		$dbc = new DbConnector();
		$dbc->connectPDO();
		
		$this->_dossierId = $dbc->getDossierToProcess($projectId);
	}
	
	public function getEnvironment()
	{
		//THIS MUST BE READ FROM DATABASE
		switch ($this->getProjectId())
		{
			case 1:
				return 'PROD';
			case 2:
				return 'GOLD';
			default:
				return 'GOLD';
		}
	}
	
	public function getProjectId()
	{
		return $this->_projectId;
	}
	
	public function getDossierId()
	{
		return $this->_dossierId;
	}
	
}

Class UrlParameters implements SettingsProvider
{
	private $_getString = null;
	
	public function __construct($get)
	{
		$this->_getString = $get;
	}
	
	private function _getFromQueryString($key, $default = '')
	{
		if (!array_key_exists($key, $this->_getString))
		{
			return $default;
		} 
		else
		{
			return $this->_getString[$key];
		}
	}
	
	/**
	*	Returns the current selected environemnt passed by query string; if not specified, default is PROD
	*	@return String
	*/
	public function getEnvironment()
	{
		
		//THIS MUST BE READ FROM DATABASE
		switch ($this->getProjectId())
		{
			case 1:
				return 'PROD';
			case 2:
				return 'GOLD';
			default:
				return 'GOLD';
		}
		
		//AT THE MOMENT THE ENV VARIABLE FROM QUERY STRING IS IGNORED IN PLACE OF PROJECT ID
		return $this->_getFromQueryString(URL_ENVIRONMENT, 'PROD');
	}
	
	/**
	*	Returns the current project Id passed by query string; if not specified, default is 1
	*	@return String
	*/
	public function getProjectId()
	{
		return $this->_getFromQueryString(URL_PROJECTID, '1');
	}
	
	/**
	*	Returns the Dossier Id passed by query string; if not specified, default is 1
	*	@return String
	*/
	public function getDossierId()
	{
		return $this->_getFromQueryString(URL_DOSSIERID);
	}
	
}

Class Settings
{
	/*@var UrlParameters*/
	//private $_up;
	
	/*@var SettingsProvider*/
	private $_settingsProvider;
	
	private $_envConArray = [
		'PROD' => [
			'CUSTOMER_LOGIN' => 'ferrsimo',
			'CUSTOMER_NUMBER' => '3133320',
			'CUSTOMER_SIGNATURE' => 'akEwRUF3TUNZYmR4V0VTK2lxOWd5U3JDN0I3a2pIQWJMN2t4RUx4ZmRDTEdWUHcyYUhueWdubnc0dHFRT1J4bDg2R2VpblB3NERZbHNQdz0=',
			'INTERFACE_PARTNER_NUMBER' => '3131411',
			'INTERFACE_PARTNER_SIGNATURE' => 'jA0EAwMCrjjoCCEuBBBgySvQtBjnp+NKnkteqoMkkzMRZlXddAQsnEG1vdDRAyRfRq+a41mrUtxJrmOg'
		],
		'GOLD' => [
			'CUSTOMER_LOGIN' => 'AxaIT002',
			'CUSTOMER_NUMBER' => '3132671',
			'CUSTOMER_SIGNATURE' => 'akEwRUF3TUNZYnpjU0J3WlI1dGd5U3Bjc04zWXJzakEyWjJmV3Nhdk8rcmMrQnIyc1dEWEIwS3Q5c1p5MFhnWlJYb1lES0FaQjBrSVdKRT0=',
			'INTERFACE_PARTNER_NUMBER' => '3132952',
			'INTERFACE_PARTNER_SIGNATURE' => 'AA1B25142947B830DD57B58409E479409DA18EADD5866E0DC0AB9D0F8BD0419B'
		],
		
	];
	
	public function setSettingsProvider(SettingsProvider $settingsProvider)
	{
		$this->_settingsProvider = $settingsProvider;
	}
	
	/*public function setUrlParams(UrlParameters $urlParams)
	{
		$this->_up = $urlParams;
	}*/
	
	public function getDossierId()
	{
		return $this->_settingsProvider->getDossierId();
	}
	
	public function getEnvironment()
	{
		return $this->_settingsProvider->getEnvironment();
	}
	
	public function getProjectId()
	{
		return $this->_settingsProvider->getProjectId();
	}
	
	public function getCustomerLogin()
	{
		return $this->_envConArray[$this->getEnvironment()]['CUSTOMER_LOGIN'];
	}
	
	public function getCustomerNumber()
	{
		return $this->_envConArray[$this->getEnvironment()]['CUSTOMER_NUMBER'];
	}
	
	public function getCustomerSignature()
	{
		return $this->_envConArray[$this->getEnvironment()]['CUSTOMER_SIGNATURE'];
	}
	
	public function getInterfaceParnerNumber()
	{
		return $this->_envConArray[$this->getEnvironment()]['INTERFACE_PARTNER_NUMBER'];
	}
	
	public function getInterfaceParnerSignature()
	{
		return $this->_envConArray[$this->getEnvironment()]['INTERFACE_PARTNER_SIGNATURE'];
	}
	
	public function getEnvURL()
	{
		switch ($this->getEnvironment()) 
		{
			case 'PROD':
				return 'www.dat.de';
			case 'PROD-NON-CDN':
				return 'noncdn-www.datgroup.com';
			case 'GOLD':
			default:
				return 'gold.dat.de';
		}		
	}
}

Class DownloadDossierSettings extends Settings
{
	
}

Class WebServiceConnector
{
	private $_client;
	private $_env;
	
	private $_settings;
	
	public function setSettings(Settings $settings)
	{
		$this->_settings = $settings;
		
		
	}
	
	public function getHeader()
	{
		$a = array( 
			'http'=>array( 
				'protocol_version' => 1.0,
				'header'=>"customerLogin: ".$this->_settings->getCustomerLogin()."\r\n" . 
						  "customerNumber: ".$this->_settings->getCustomerNumber()."\r\n" .
						  "customerSignature: ".$this->_settings->getCustomerSignature()."\r\n" .
						  "interfacePartnerNumber: ".$this->_settings->getInterfaceParnerNumber()."\r\n" .
						  "interfacePartnerSignature: ".$this->_settings->getInterfaceParnerSignature()."\r\n"
				)
			);
			
		return $a;
	}
	
	public function createMyClaimExternalService()
	{
		$opts = stream_context_create($this->getHeader());
		$link = 'http://'.$this->_settings->getEnvURL().'/myClaim/soap/v2/MyClaimExternalService?wsdl';
		$params = array('stream_context' => $opts, 'exceptions'=>true, 'trace'=>1, 'cache_wsdl' => 'WSDL_CACHE_NONE');
		
		$this->_client = new SoapClient($link, $params);
		
	}
	
	public function performListContract($pageLimit, $pageOffset) {
		
		if ((is_numeric($pageLimit)) and (is_numeric($pageOffset))) 
		{
			$pageOffset = $pageLimit * $pageOffset;
		}
		
		$listContracts =  "<myc:listContracts xmlns:myc='http://www.dat.eu/myClaim/soap/v2/MyClaimExternalService'>
						 <pageLimit>".$pageLimit."</pageLimit>
						 <pageOffset>".$pageOffset."</pageOffset>
						 </myc:listContracts>";
	
		$listContracts = new SoapVar($listContracts, XSD_ANYXML);
		try 
		{
			$listContracts = $this->_client->__soapCall("listContracts", array($listContracts));
			} catch(SoapFault $e)
		{
			echo "ERROR ".$e->getMessage();
		}
		
		return $listContracts;
	}
	
	public function performGetContract($dossierId)
	{
		try
		{
			$params =array('contractId' => $dossierId, 'isWithHistoricalCalculations' => 0);
			$dossier = $this->_client->__soapCall("getContract", array('parameters' => $params));

		} catch(SoapFault $e){
			echo("ERROR: ".$e->faultstring."<br>");
			echo($this->_client->__getLastRequest());
		}
		
		return $dossier->return;
		//return $dossier;
	}
	
	public function performListContractHistory($paramsHistory)
	{
		try
		{
			$dossierHistories=$this->_client->__soapCall("listContractHistory", array('parameters' => $paramsHistory));
		} catch(SoapFault $e)
		{
			echo("ERROR: ".$e->faultstring."<br>");
		}
		
		return $dossierHistories;
	}
}

class DbConnector
{
	private $_conn;
	
	public function connect()
	{
		$this->_conn = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DBNAME); 
	}
	
	/*
	public function connectPDO()
	{
		$this->_conn = new PDO("mysql:host=".DB_HOSTNAME.";dbname=".DB_DBNAME,DB_USERNAME,DB_PASSWORD);
		$this->_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	}
	*/
	
	public function getJobs()
	{
		$sql = "select * from job";
		$statement = $this->_conn->prepare($sql);
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_NUM);
	}
	
	public function getOffsetToProcess($projectId)
	{
		try 
		{
			$this->_conn->beginTransaction();
			
			/*get job number*/
			$sql = "SELECT assign_offset($projectId) as assigned_offset";
			$stmt = $this->_conn->prepare($sql);
			$stmt->execute();
			$result = $stmt->fetchAll(PDO::FETCH_OBJ);
			$assigned_offset = $result[0]->assigned_offset;
			
			$this->_conn->commit();
			
			return $assigned_offset;
		}
		catch (PDOException $e) 
		{
			$this->_conn->rollBack();
			echo "ERROR ".$e->getMessage();
			return -1;
		}
	}
	
	public function addContractToJobs($projectId, $contractId, $lastUpdate)
	{
		$sqla=array();
	
		try 
		{
			$this->_conn->beginTransaction();
			
			/*get job number*/
			$sql = "SELECT * FROM `job` WHERE `project_id`=$projectId AND `dossier_id`=$contractId";
			$stmt = $this->_conn->prepare($sql);
			$stmt->execute();
			$result = $stmt->fetchAll(PDO::FETCH_OBJ);

			$sqla[] = $sql;
			
			if (empty($result))
			{				
				$sql = "INSERT INTO `job`(`project_id`,`dossier_id`, `last_update`) VALUES ('$projectId','$contractId','$lastUpdate')";
				$stmt = $this->_conn->prepare($sql);
				$stmt->execute();
				
				$sqla[] = $sql;
			}
			else
			{
				if ($result[0]->last_update != $lastUpdate)
				{
					$sql = "UPDATE `job` SET `last_update`='$lastUpdate' WHERE `project_id`=$projectId AND `dossier_id`='$contractId'";
					$stmt = $this->_conn->prepare($sql);
					$stmt->execute();
					$sqla[] = $sql;		
				}
			}
			
			print_r($sqla);
			
			$this->_conn->commit();
			
			return 1;
		}
		catch (PDOException $e) 
		{
			$this->_conn->rollBack();
			echo "ERROR ".$e->getMessage();
			return -1;
		}
	}
	
	public function startOverAgain($projectId)
	{
		try 
		{
			$this->_conn->beginTransaction();
			
			$sql = "call restart_from_page_one($projectId);";
			$stmt = $this->_conn->prepare($sql);
			$stmt->execute();
			
			$this->_conn->commit();
		}
		catch (PDOException $e) 
		{
			$this->_conn->rollBack();
			echo "ERROR ".$e->getMessage();
		}
	}
	
	public function getDossierToProcess($projectId)
	{
		$dossier_id = -1;
		try 
		{
			$this->_conn->beginTransaction();
			
			/*assign a job number as RUNNING */
			$sql = "SELECT assign_job($projectId) as jobId";
			$stmt = $this->_conn->prepare($sql);
			$stmt->execute();
			
			$result = $stmt->fetchAll(PDO::FETCH_OBJ);
			$jobId = $result[0]->jobId;
			
			/*select dossier associated to that job*/
			$sql = "SELECT dossier_id from job where project_id =$projectId and id=$jobId";
			$stmt = $this->_conn->prepare($sql);
			$stmt->execute();
			
			$result = $stmt->fetchAll(PDO::FETCH_OBJ);
			$dossier_id = $result[0]->dossier_id;
			
			$this->_conn->commit();
		}
		catch (PDOException $e) 
		{
			$this->_conn->rollBack();
			echo "ERROR ".$e->getMessage();
		}
		
		return $dossier_id;
	}
	
	public function setJobDone($dossierId, $projectId)
	{
		$dossier_id = -1;
		try 
		{
			$this->_conn->beginTransaction();
			
			$sql="CALL set_job_done($dossierId, $projectId);";
			$stmt = $this->_conn->prepare($sql);
			$stmt->execute();
			$this->_conn->commit();
			
			echo "OK";
		}
		catch (PDOException $e) 
		{
			$this->_conn->rollBack();
			echo "ERROR ".$e->getMessage();
		}
		
		return $dossier_id;
	}
	
	public function insertDossier(Dossier $dossier)
	{
		$sqla=array();
		try 
		{
			$this->_conn->beginTransaction();
			
			$sql = "SELECT * FROM `dossier` WHERE id=".$dossier->getId();
			$stmt = $this->_conn->prepare($sql);
			$stmt->execute();
			
			$sqla[] = $sql;
			
			if ($stmt->rowCount() == 1)
			{
				$sql = "DELETE FROM `dossier` WHERE id=".$dossier->getId();
				$stmt = $this->_conn->prepare($sql);
				$stmt->execute();	
				$sqla[] = $sql;
			}
			
			"Inserisco DOSSIER<BR>";
			$sql  = "INSERT INTO `dossier` VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			$stmt = $this->_conn->prepare($sql);
			$stmt->execute($dossier->toPDOarray());

			$sqla[] = $sql;
			
			"Inserisco POSIZIONI MATERIALI<BR>";
			$sql = "INSERT INTO material_position VALUES (?,?,?,?,?,?,?,?,?,?,?)";
			
			foreach ($dossier->getMaterialPositions() as $mp)
			{
				$stmt = $this->_conn->prepare($sql);
				$stmt->execute($mp->toPDOarray());
			}
			$sqla[] = $sql;
			
			$this->_conn->commit();
			//die;
		}
		catch (PDOException $e) 
		{
			$this->_conn->rollBack();
			echo "ERROR ".$e->getMessage();
		}
	}
	
	public function updMaterialPositions()
	{
		$sqla=array();
		try 
		{
			//$this->_conn->beginTransaction();
			
			$sql = "SELECT id, DossierId FROM `job`";
			$stmt = $this->_conn->prepare($sql);
			$stmt->execute();
			
			$result = $stmt->fetchAll(PDO::FETCH_OBJ);
			
			$i=0;
			foreach ($result as $obj)
			{
				$sql = "UPDATE material_position SET job_id=".$obj->id." WHERE DossierId = ".$obj->DossierId. " and job_id = 0";
				$stmt = $this->_conn->prepare($sql);
				$stmt->execute();
				$i++;
			}
			echo "$i job aggiornati";
			die;
			
			$sqla[] = $sql;
			
			if ($stmt->rowCount() == 1)
			{
				$sql = "DELETE FROM `dossier` WHERE DossierId=".$dossier->getId();
				$stmt = $this->_conn->prepare($sql);
				$stmt->execute();	
				$sqla[] = $sql;
			}
			
			$sql  = "INSERT INTO `dossier` VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			$stmt = $this->_conn->prepare($sql);
			$stmt->execute($dossier->toPDOarray());

			$sqla[] = $sql;
			
			$sql = "INSERT INTO material_position VALUES (?,?,?,?,?,?,?,?,?,?)";
			foreach ($dossier->getMaterialPositions() as $mp)
				$this->_conn->prepare($sql)->execute($mp->toPDOarray());

			$sqla[] = $sql;

			print_r($sqla);
			
			$this->_conn->commit();
		}
		catch (PDOException $e) 
		{
			$this->_conn->rollBack();
			echo "ERROR ".$e->getMessage();
		}
	}
}


class ListContract 
{
	private $_pageOffset;
	private $_pageLimit;
	private $_environment;
	//private $_dbc;
	private $_wsc;
	
	public function __construct($pageLimit, $pageOffset, $environment, Settings $settings)
	{
		$this->_pageOffset = $pageOffset;
		$this->_pageLimit  = $pageLimit;
		$this->_environment = $environment;
		
		
		//$this->_dbc = new DbConnector();
		$this->_wsc = new WebServiceConnector($environment);
		$this->_wsc->setSettings($settings);
	}
	
	public function perform()
	{
		//$this->_dbc->connect();
		$this->_wsc->createMyClaimExternalService();
		
		$listContracts = $this->_wsc->performListContract($this->_pageLimit, $this->_pageOffset);
		
		//If there are no more contracts, stop the script
		//if(!isset($listContracts->return)) break;
		unset($listContractsArray);
		
		if (!property_exists($listContracts, "return")) 
		{
			var_dump($listContracts);
			echo '</br><hr>';
			return null;
		}
		
		if(count($listContracts->return)>1) 
			$listContractsArray=$listContracts->return;
		else 
			$listContractsArray[]=$listContracts->return;
		
		return $listContractsArray;
	}
}

class MaterialPosition
{
	private $_dossierId;
	private $_projectId = -1;
	private $_rowNum;
	private $_dvn = '';
	private $_referenceSparePart = '';
	private $_description = '';
	private $_sparePartPrice = '';
	private $_modifiedSparePartReference = '';
	private $_modifiedSparePartDescription = '';
	private $_modifiedSparePartPrice = '';
	private $_modifiedSparePartBSComment = '';
	
	public function __construct($projectId, $dossierId, $rowNum, $materialPositions)
	{
		$this->_dossierId = $dossierId;
		$this->_projectId = $projectId;
		$this->_rowNum = $rowNum;
		
		if (property_exists($materialPositions, 'DATProcessId'))
			$this->_dvn = $materialPositions->DATProcessId->_;
		if (property_exists($materialPositions, 'PartNumber'))
			$this->_referenceSparePart = $materialPositions->PartNumber->_;
		if (property_exists($materialPositions, 'Description'))
			$this->_description = $materialPositions->Description->_;
		if (property_exists($materialPositions, 'ValueTotalCorrected'))
			$this->_sparePartPrice = $materialPositions->ValueTotalCorrected->_;
	}
	
	private function safeAssign($object, $property, $dafaultValue = '')
	{
		
	}
	
	public function getDVN()
	{
		if ($this->_dvn != 0)
			return $this->_dvn;
		else
		{
			// if this position is manual has no DVN, n this case a fake unique DVN is returned
			return '999' . str_pad($this->_rowNum,3,'0', STR_PAD_LEFT);
		}
	}
	
	public function setReferenceSparePart($reference)
	{
		$this->_referenceSparePart = $reference;
	}
	
	public function getReferenceSparePart()
	{
		return $this->_referenceSparePart;
	}
	
	public function setDescription($description)
	{
		$this->_description = $description;
	}
	
	public function getDescription()
	{
		return $this->_description;
	}
	
	public function setSparePartPrice($price)
	{
		$this->_sparePartPrice = $price;
	}
	
	public function getSparePartPrice()
	{
		return $this->_sparePartPrice;
	}
	
	public function setModifiedAspisSparePartReference($reference)
	{
		$this->_modifiedSparePartReference = $reference;
	}
	
	public function getModifiedAspisSparePartReference()
	{
		return $this->_modifiedSparePartReference;
	}
	
	public function setModifiedAspisSparePartDescription($modifiedSparePartDescription)
	{
		$this->_modifiedSparePartDescription = $modifiedSparePartDescription;
	}
	
	public function getModifiedAspisSparePartDescription()
	{
		return $this->_modifiedSparePartDescription;
	}
	
	public function setModifiedAspisSparePartPrice($modifiedSparePartPrice)
	{
		$this->_modifiedSparePartPrice = $modifiedSparePartPrice;
	}
	public function getModifiedAspisSparePartPrice()
	{
		return $this->_modifiedSparePartPrice;
	}
	
	public function setModifiedAspisSparePartBSComment($modifiedSparePartBSComment = 'N/D')
	{
		$this->_modifiedSparePartBSComment = $modifiedSparePartBSComment;
	}
	
	/**
	*	Returns the comment entered by BodyShop for not using Aspis Spare Parts
	*/
	public function getModifiedAspisSparePartBSComment()
	{
		return $this->_modifiedSparePartBSComment;
	}
	
	public function toPDOarray()
	{
		return [
			$this->_dossierId,
			$this->_projectId,
			$this->_rowNum,
			$this->_dvn,
			$this->_referenceSparePart,
			$this->_description,
			$this->_sparePartPrice,
			$this->getModifiedAspisSparePartReference(),
			$this->getModifiedAspisSparePartDescription(),
			$this->getModifiedAspisSparePartPrice(),
			$this->getModifiedAspisSparePartBSComment()
		];
	}
}

class Dossier
{
	const REF_INDEX = 2;
	const DES_INDEX = 3;
	const PRC_INDEX = 6;
	const CMT_INDEX = 13;

	private $_dossier;	// the original XML dossier
	private $_projectId = -1;
	private $_customTemplateData = array();
	private $_complexTemplateData = array();
	
	private $_dossierHistories;
	private $_entryDate = null;
	private $_aspisBsId = null;
	
	private $_materialPositions = array();
	private $_memoAscale2Array = null;
	private $_memoAchangedArray = null;
	
	public function __construct($projectId, $dossierXml, $dossierHistory = null)
	{
		$this->_projectId = $projectId;
		//$this->_dossier = $dossier->return;
		$this->_dossier = $dossierXml;
		$this->_exctractCustomTemplateData();
		$this->_extractMaterialPositions();
		
		if (!is_null($dossierHistory))
		{
			$this->setDossierHistories($dossierHistory);
		}
	}
	
	public function getMaterialPositions()
	{
		return $this->_materialPositions;
	}
	
	public static function hasMaterialPositions($dossierXml)
	{
		if (!isset($dossierXml->Dossier->RepairCalculation->CalcResultCustomerRanking)) return false;
		
		return isset($dossierXml->Dossier->RepairCalculation->CalcResultCustomerRanking->MaterialPositions->MaterialPosition);
	}
	
	private function _exctractCustomTemplateData()
	{
		if (!empty($this->_customTemplateData)) return;
		$this->_customTemplateData = array(
			'assignmentID'=>'',
			'AspisBodyshopId'=>'',
			'DamageNumber'=>'',
			'AspisAssessmentNumber'=>'',
			'LicenseNumber'=>'',
			'Opponent_LicenseNumber'=>'',
			'memoAscale2'=>'',
			'memoAchanged'=>'',
			'CompanyName'=>'',
			'numeroManutenzione' =>'',
			'operatoreOCS' => '',
			'OperatoreDAT' => '',
			'LicenseNumber' => '',
			'tipoAutorizzazione' => '',
			'tipoIntervento' => '',
			'codiceIntervento' => '',
			'totaleGomme' => '',
			'tipoGomme' => '',
			'dataRicovero' => '',
			'dataPrevistaRestituzione2' => '',
			'numeroSinistro2' => '',
			'mileageOdometer' => '',
			'totaleImponibile' => '',
			'descrizioneDanni' => '',
			'numeroAutorizzazione' => '',
			'identificationNumber' => ''
		);
		
		$dossier = $this->_dossier;
		foreach($dossier->customTemplateData->entry as $custom)
		{
			if (array_key_exists($custom->key, $this->_customTemplateData))
			{
				$this->_customTemplateData[$custom->key] = $custom->value;
			}
		}		
	}
	
	private function _createMaterialPosition($dossierId, $rowNum, $materialPositionElement)
	{
		$newMp = new MaterialPosition($this->_projectId, $dossierId, $rowNum, $materialPositionElement);
		
		$dvn = $newMp->getDVN();
		
		$memoAchanged = $this->getMemoAchangedToArray();
		$memoAscale2  = $this->getMemoAscale2ToArray();
		
		if (array_key_exists($dvn, $memoAchanged) and array_key_exists($dvn, $memoAscale2))
		{
			if ($memoAchanged[$dvn][self::REF_INDEX] != $memoAscale2[$dvn][self::REF_INDEX])
				{
					$newMp->setReferenceSparePart($memoAscale2[$dvn][self::REF_INDEX]);
					$newMp->setDescription       ($memoAscale2[$dvn][self::DES_INDEX]);
					$newMp->setSparePartPrice    ($memoAscale2[$dvn][self::PRC_INDEX]);
					
					$newMp->setModifiedAspisSparePartReference  ($memoAchanged[$dvn][self::REF_INDEX]);
					$newMp->setModifiedAspisSparePartDescription($memoAchanged[$dvn][self::DES_INDEX]);
					$newMp->setModifiedAspisSparePartPrice      ($memoAchanged[$dvn][self::PRC_INDEX]);
					
					$comment = '-'; //if a comment does not exists, array offset 13 is not defined -> ask SIMONE LATINA why
					if (array_key_exists(self::CMT_INDEX, $memoAchanged[$dvn]))
					{
						$comment = $memoAchanged[$dvn][self::CMT_INDEX];
					}
					$newMp->setModifiedAspisSparePartBSComment($comment);
				}
		}
		else
		{
			//throe an exception???
		}
		return $newMp;
			
	}
	
	private function _extractMaterialPositions()
	{
		//if the array _materialPositions is not empy means this operation has already been done and it has no sense to do that again
		if (!empty($this->_materialPositions)) return;
		
		$this->_materialPositions = array();
		
		if (!Dossier::hasMaterialPositions($this->_dossier)) return;
		
		$dossierId = $this->getId();
		$mps = $this->_dossier->Dossier->RepairCalculation->CalcResultCustomerRanking->MaterialPositions->MaterialPosition;
		
		$rowNum = 1;
		// if it is an object means it is a single entry, if it is an array has multiple entries
		if (is_object($mps)) 
		{
			$newMp = $this->_createMaterialPosition($dossierId, $rowNum++, $mps);
			$this->_materialPositions[$newMp->getDVN()] = $newMp;
		}
		else 
			foreach ($mps as $mp)
			{
				$newMp = $this->_createMaterialPosition($dossierId, $rowNum++, $mp);
				$this->_materialPositions[$newMp->getDVN()] = $newMp;
			}
		
	}
	
	public function setDossierHistories($dossierHistories)
	{
		$this->_dossierHistories = $dossierHistories;
	}
	
	public function getId()
	{
		return $this->_dossier->Dossier->DossierId->_;
	}
	
	public function getAspisBsId(){
		//following line is deprecated
		//return $this->_customTemplateData['assignmentID'];
	
		if (!is_null($this->_aspisBsId)) return $this->_aspisBsId;
		
		$this->_aspisBsId = '';
		$ctd = $this->_dossier->complexTemplateData->field;
		foreach ($ctd as $field)
		{
			if ($field->name == 'AspisBodyshopId')
			{
				$this->_aspisBsId = $field->stringValue;
				break;
			}
		}
		
		return $this->_aspisBsId;
	}
	
	public function getCreateDate()
	{
		return $this->_dossier->Dossier->CreateDate->_;
	}
	
	public function getLastUpdate()
	{
		return $this->_dossier->Dossier->ChangeDate->_;
	}
	
	public function getStatus()
	{
		return $this->_dossier->statusName;
	}
	
	public function getVIN()
	{
		return $this->_dossier->Dossier->Vehicle->VehicleIdentNumber->_;
	}
	
	public function getDatECode()
	{
		return $this->_dossier->Dossier->Vehicle->DatECode->_;
	}
	
	public function getConstructionTime()
	{
		return $this->_dossier->Dossier->Vehicle->ConstructionTime->_;
	}
	
	public function getAssignmentID()
	{
		return $this->_customTemplateData['assignmentID'];
	}
	
	public function getDamageNumber()
	{
		return $this->_customTemplateData['DamageNumber'];
	}
	
	public function getAspisAssessmentNumber()
	{
		return $this->_customTemplateData['AspisAssessmentNumber'];
	}
	
	public function getLicenseNumber()
	{
		return $this->_customTemplateData['LicenseNumber'];
	}
	
	public function getOpponentLicenseNumber()
	{
		return $this->_customTemplateData['Opponent_LicenseNumber'];
	}
	
	public function getMemoAscale2()
	{
		return $this->_customTemplateData['memoAscale2'];
	}
	
	public function getMemoAchanged()
	{
		return $this->_customTemplateData['memoAchanged'];
	}
	
	public function getTemplateData($fieldName)
	{
		return $this->_customTemplateData[$fieldName];
	}
	
	public function gettotaleGomme()
	{
		return str_replace('.',',',$this->_customTemplateData['totaleGomme']);
	}
	
	public function gettotaleimponibile()
	{
		return str_replace('.',',',$this->_customTemplateData['totaleImponibile']);
	}
	
	public function getMemoAchangedToArray()
	{
		if (is_null($this->_memoAchangedArray))
		{
			$jsonArray = json_decode($this->getMemoAchanged());
			$jsonArray2 = array();
			
			if (!is_null($jsonArray))
			{
				foreach ($jsonArray as $key => $subArray)
				{
					$dvn = $subArray[1];
					$jsonArray2[$dvn] = $subArray;
				}
				
				unset($jsonArray);
			}
			$this->_memoAchangedArray = $jsonArray2;
		}
		return $this->_memoAchangedArray;
	}
	
	public function getMemoAscale2ToArray()
	{
		if (is_null($this->_memoAscale2Array))
		{
			$jsonArray = json_decode($this->getMemoAscale2());
			$jsonArray2 = array();
			
			if (!is_null($jsonArray))
			{
				foreach ($jsonArray as $key => $subArray)
				{
					$dvn = $subArray[1];
					$jsonArray2[$dvn] = $subArray;
				}
				
				unset($jsonArray);
			}
			$this->_memoAscale2Array = $jsonArray2;
		}
		return $this->_memoAscale2Array;
	}

	/*FB - myClaim hystory is represented with a stack where first element is the more recent one*/
	const GET_YOUNGER = true;
	const GET_OLDER = false;
	public function getEntryDate($mode = self::GET_YOUNGER)
	{
		if (is_null($this->_dossierHistories)) return '';
		
		if (is_null($this->_entryDate))
		{
			$this->_entryDate = '';
			foreach ($this->_dossierHistories->return as $dossierHistory) {
				if (!property_exists($dossierHistory, 'statusName')) continue;
				$statusName = $dossierHistory->statusName;
				if ($statusName == 'Calcolato') {
					$this->_entryDate = $dossierHistory->entryDate;
					if ($mode == self::GET_YOUNGER) break;
					if ($mode == self::GET_OLDER)   continue;
				}
			}
		}
		
		return $this->_entryDate;
	}
	
	public function setProjectId($projectId)
	{
		$this->_projectId = $projectId;
	}
	
	public function getProjectId()
	{
		return $this->_projectId;
	}
	
	public function getCompanyName()
	{
		return $this->_customTemplateData['CompanyName'];
	}
	
	public function toPDOarray()
	{
		return [
			$this->getId(),
			$this->getProjectId(),
			$this->getAspisBsId(),
			$this->getAssignmentID(),
			$this->getAspisAssessmentNumber(),
			$this->getDamageNumber(),
			$this->getEntryDate(),
			$this->getCreateDate(),
			$this->getLastUpdate(),
			$this->getStatus(),
			$this->getVIN(),
			$this->getLicenseNumber(),
			$this->getOpponentLicenseNumber(),
			$this->getDatECode(),
			$this->getConstructionTime(),
			$this->getCompanyName()
		];
	}
}

class GetContract
{
	private $_dossierId;
	private $_projectId;
	
	/* @var WebServiceConnector */
	private $_wsc;
	
	public function __construct(Settings $settings)
	{
		//$this->_dossierId = $dossierId;
		$this->_dossierId = $settings->getDossierId();
		$this->_projectId = $settings->getProjectId();
		
		$this->_wsc = new WebServiceConnector();
		$this->_wsc->setSettings($settings);
	}
	
	public function perform()
	{
		$this->_wsc->createMyClaimExternalService();
		$dossierXml = $this->_wsc->performGetContract($this->_dossierId);
		//if (!isset($dossierXml->return->Dossier->RepairCalculation->CalcResultCustomerRanking->MaterialPositions->MaterialPosition))
		if(Dossier::hasMaterialPositions($dossierXml))
		{
			//$paramsHistory=array('contractId' => $dossierXml->return->Dossier->DossierId->_);
			$paramsHistory=array('contractId' => $dossierXml->Dossier->DossierId->_);
			$dossierHistories = $this->_wsc->performListContractHistory($paramsHistory);
		}
		else
		{
			$dossierHistories = null;
			echo 'NO MATERIAL POSITIONS';
			//return new EmptyDossier();
		}
		
		return new Dossier($this->_projectId, $dossierXml, $dossierHistories);
	}
	
}

Class EmptyDossier extends Dossier
{
	public function __construct()
	{
	}
}

class Job
{
	//private $_
}
