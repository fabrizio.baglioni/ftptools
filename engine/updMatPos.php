<?php
include_once'AXA_export.php';

$env = 'PROD';
$dbc = new DbConnector();
$dbc->connectPDO();

$dbc->updMaterialPositions();
die;

$pageLimit=100;
if (isset($_GET["pageOffset"])) $pageOffset=$_GET["pageOffset"];

if (isset($_GET["pageOffset"])) 
	$pageOffset=$_GET["pageOffset"];
else
	$pageOffset = $dbc->getOffsetToProcess();

echo "Processing offset $pageOffset";
echo '</br>';

$lc = new ListContract($pageLimit, $pageOffset, $env);
$contracts = $lc->perform();

if ($contracts == null) //this means that we went over the last dossier; the inbox is over, so we have to start from dossier 1 again
{
	$dbc->startOverAgain();
	die('Reached last dossier, re-starting from page one');
}

$contracts_added = '';
foreach($contracts as $contract)
{
	//If there is no calculation, we don't need to check the dossier
	if(!isset($contract->totalDamage) || (int)$contract->totalDamage==0) continue;
	echo 'addContractToJobs: ';
	echo '</br>';
	echo '<hr>';
	$dbc->addContractToJobs($contract->contractId, $contract->contractModified);
	$contracts_added .= $contract->contractId.',';
}
