-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Nov 25, 2019 alle 17:01
-- Versione del server: 10.1.38-MariaDB
-- Versione PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbs208153`
--

DELIMITER $$
--
-- Procedure
--

USE `dbs208153`$$

CREATE  PROCEDURE `restart_from_page_one` (IN `projectId` INT)  MODIFIES SQL DATA
    DETERMINISTIC
BEGIN
 UPDATE counter
 SET value = 0,
 last_update = current_timestamp()
 WHERE counter='last_processed_offset' AND project_id = projectId;
 
 UPDATE counter
 SET value = value + 1,
 last_update = current_timestamp()
 WHERE counter='rounds' AND project_id = projectId;
END$$

CREATE  PROCEDURE `set_job_done` (IN `dossierId` INT UNSIGNED, IN `projectId` INT)  MODIFIES SQL DATA
    DETERMINISTIC
UPDATE job
SET status = 'DONE',
status_change = current_timestamp()
WHERE dossier_id = dossierId and project_id = projectId$$

--
-- Funzioni
--
CREATE  FUNCTION `assign_job` (`projectId` SMALLINT) RETURNS INT(11) MODIFIES SQL DATA
    DETERMINISTIC
BEGIN

 DECLARE job_id INT;

 SELECT min(id)
 INTO job_id
 FROM job
 WHERE status='READY' and project_id = projectId
 FOR UPDATE;

 UPDATE job
 SET status = 'RUNNING',
 status_change = current_timestamp(),
 processed_times = processed_times + 1
 WHERE id = job_id;

 return job_id;
END$$

CREATE  FUNCTION `assign_offset` (`projectId` INT UNSIGNED) RETURNS INT(11) MODIFIES SQL DATA
    DETERMINISTIC
BEGIN

 DECLARE offset_to_process INT;

 SELECT max(value)
 INTO offset_to_process
 FROM counter c
 WHERE c.counter='last_processed_offset' AND
 `project_id`=projectId
 FOR UPDATE;

 SET offset_to_process = offset_to_process + 1;
 
 UPDATE counter c
 SET c.value = offset_to_process,
 c.last_update = current_timestamp()
 WHERE c.counter='last_processed_offset' AND
 `project_id`=projectId;

 return offset_to_process;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `axa_claims_gold`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `axa_claims_gold` (
`id` int(11)
,`project_id` smallint(5) unsigned
,`AspisBS_ID` mediumint(8) unsigned
,`assignmentID` text
,`AspisAssessmentNumber` text
,`DamageNumber` text
,`Last_Calculation_Date` datetime
,`Assessment_Creation_Date` datetime
,`Assessment_Last_Update_Date` datetime
,`Status` text
,`VIN` text
,`Number_Plate` text
,`Opponent_Number_Plate` text
,`Ecode` text
,`ConstructionTime` int(11)
,`row_number` tinyint(3) unsigned
,`dvn` int(11)
,`description` text
,`Reference_Spare_Part` text
,`Spare_Part_Price` double
,`Modified_SP_Reference` text
,`Modified_SP_Desc` varchar(100)
,`Modified_SP_Price` double
,`Modified_SP_BS_Comment` varchar(100)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `axa_claims_prod`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `axa_claims_prod` (
`id` int(11)
,`project_id` smallint(5) unsigned
,`AspisBS_ID` mediumint(8) unsigned
,`assignmentID` text
,`AspisAssessmentNumber` text
,`DamageNumber` text
,`Last_Calculation_Date` datetime
,`Assessment_Creation_Date` datetime
,`Assessment_Last_Update_Date` datetime
,`Status` text
,`VIN` text
,`Number_Plate` text
,`Opponent_Number_Plate` text
,`Ecode` text
,`ConstructionTime` int(11)
,`row_number` tinyint(3) unsigned
,`dvn` int(11)
,`description` text
,`Reference_Spare_Part` text
,`Spare_Part_Price` double
,`Modified_SP_Reference` text
,`Modified_SP_Desc` varchar(100)
,`Modified_SP_Price` double
,`Modified_SP_BS_Comment` varchar(100)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `counter`
--

CREATE TABLE `counter` (
  `project_id` smallint(5) UNSIGNED NOT NULL,
  `counter` varchar(40) NOT NULL,
  `value` int(11) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `counter`
--

INSERT INTO `counter` (`project_id`, `counter`, `value`, `last_update`) VALUES
(1, 'last_processed_offset', 353, '2019-11-25 16:01:13'),
(1, 'rounds', 9, '2019-11-02 16:32:21');

-- --------------------------------------------------------

--
-- Struttura della tabella `dossier`
--

CREATE TABLE `dossier` (
  `id` int(11) NOT NULL,
  `project_id` smallint(5) UNSIGNED NOT NULL,
  `AspisBS_ID` mediumint(8) UNSIGNED DEFAULT NULL,
  `assignmentID` text NOT NULL,
  `AspisAssessmentNumber` text NOT NULL,
  `DamageNumber` text NOT NULL,
  `Last_Calculation_Date` datetime NOT NULL,
  `Assessment_Creation_Date` datetime DEFAULT NULL,
  `Assessment_Last_Update_Date` datetime DEFAULT NULL,
  `Status` text NOT NULL,
  `VIN` text NOT NULL,
  `Number_Plate` text NOT NULL,
  `Opponent_Number_Plate` text NOT NULL,
  `Ecode` text NOT NULL,
  `ConstructionTime` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `dossier`
--


-- --------------------------------------------------------

--
-- Struttura della tabella `job`
--

CREATE TABLE `job` (
  `id` mediumint(9) UNSIGNED NOT NULL,
  `project_id` smallint(5) UNSIGNED NOT NULL,
  `dossier_id` int(10) UNSIGNED NOT NULL,
  `status` set('READY','RUNNING','DONE','') NOT NULL DEFAULT 'READY',
  `last_update` varchar(30) DEFAULT NULL,
  `status_change` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `processed_times` tinyint(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `job`
--

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `jobsstatus_prod`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `jobsstatus_prod` (
`St` varchar(7)
,`total` bigint(21)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `material_position`
--

CREATE TABLE `material_position` (
  `dossier_id` int(11) NOT NULL,
  `project_id` smallint(5) UNSIGNED NOT NULL,
  `row_number` tinyint(3) UNSIGNED NOT NULL,
  `DVN` int(11) NOT NULL,
  `Reference_Spare_Part` text NOT NULL,
  `Description` text NOT NULL,
  `Spare_Part_Price` double DEFAULT NULL,
  `Modified_SP_Reference` text NOT NULL,
  `Modified_SP_Desc` varchar(100) DEFAULT NULL,
  `Modified_SP_Price` double DEFAULT NULL,
  `Modified_SP_BS_Comment` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `material_position`
--


-- --------------------------------------------------------

--
-- Struttura della tabella `project`
--

CREATE TABLE `project` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `customer_code` mediumint(11) UNSIGNED NOT NULL,
  `environment` set('PROD','GOLD','TEST') NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `project`
--

INSERT INTO `project` (`id`, `customer_code`, `environment`, `description`) VALUES
(1, 3132952, 'PROD', 'AXA Statistics on PROD Environment'),
(2, 3132952, 'GOLD', 'AXA Statistics on GOLD Environment');

-- --------------------------------------------------------

--
-- Struttura per vista `axa_claims_gold`
--
DROP TABLE IF EXISTS `axa_claims_gold`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `axa_claims_gold`  AS  select `d`.`id` AS `id`,`d`.`project_id` AS `project_id`,`d`.`AspisBS_ID` AS `AspisBS_ID`,`d`.`assignmentID` AS `assignmentID`,`d`.`AspisAssessmentNumber` AS `AspisAssessmentNumber`,`d`.`DamageNumber` AS `DamageNumber`,`d`.`Last_Calculation_Date` AS `Last_Calculation_Date`,`d`.`Assessment_Creation_Date` AS `Assessment_Creation_Date`,`d`.`Assessment_Last_Update_Date` AS `Assessment_Last_Update_Date`,`d`.`Status` AS `Status`,`d`.`VIN` AS `VIN`,`d`.`Number_Plate` AS `Number_Plate`,`d`.`Opponent_Number_Plate` AS `Opponent_Number_Plate`,`d`.`Ecode` AS `Ecode`,`d`.`ConstructionTime` AS `ConstructionTime`,`m`.`row_number` AS `row_number`,`m`.`DVN` AS `dvn`,`m`.`Description` AS `description`,`m`.`Reference_Spare_Part` AS `Reference_Spare_Part`,`m`.`Spare_Part_Price` AS `Spare_Part_Price`,`m`.`Modified_SP_Reference` AS `Modified_SP_Reference`,`m`.`Modified_SP_Desc` AS `Modified_SP_Desc`,`m`.`Modified_SP_Price` AS `Modified_SP_Price`,`m`.`Modified_SP_BS_Comment` AS `Modified_SP_BS_Comment` from (`dossier` `d` left join `material_position` `m` on(((`d`.`project_id` = `m`.`project_id`) and (`d`.`id` = `m`.`dossier_id`)))) where (`d`.`project_id` = 2) ;

-- --------------------------------------------------------

--
-- Struttura per vista `axa_claims_prod`
--
DROP TABLE IF EXISTS `axa_claims_prod`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `axa_claims_prod`  AS  select `d`.`id` AS `id`,`d`.`project_id` AS `project_id`,`d`.`AspisBS_ID` AS `AspisBS_ID`,`d`.`assignmentID` AS `assignmentID`,`d`.`AspisAssessmentNumber` AS `AspisAssessmentNumber`,`d`.`DamageNumber` AS `DamageNumber`,`d`.`Last_Calculation_Date` AS `Last_Calculation_Date`,`d`.`Assessment_Creation_Date` AS `Assessment_Creation_Date`,`d`.`Assessment_Last_Update_Date` AS `Assessment_Last_Update_Date`,`d`.`Status` AS `Status`,`d`.`VIN` AS `VIN`,`d`.`Number_Plate` AS `Number_Plate`,`d`.`Opponent_Number_Plate` AS `Opponent_Number_Plate`,`d`.`Ecode` AS `Ecode`,`d`.`ConstructionTime` AS `ConstructionTime`,`m`.`row_number` AS `row_number`,`m`.`DVN` AS `dvn`,`m`.`Description` AS `description`,`m`.`Reference_Spare_Part` AS `Reference_Spare_Part`,`m`.`Spare_Part_Price` AS `Spare_Part_Price`,`m`.`Modified_SP_Reference` AS `Modified_SP_Reference`,`m`.`Modified_SP_Desc` AS `Modified_SP_Desc`,`m`.`Modified_SP_Price` AS `Modified_SP_Price`,`m`.`Modified_SP_BS_Comment` AS `Modified_SP_BS_Comment` from (`dossier` `d` left join `material_position` `m` on(((`d`.`project_id` = `m`.`project_id`) and (`d`.`id` = `m`.`dossier_id`)))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `jobsstatus_prod`
--
DROP TABLE IF EXISTS `jobsstatus_prod`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `jobsstatus_prod`  AS  select 'READY' AS `St`,count('dossier_id') AS `total` from `job` where ((`job`.`status` = 'READY') and (`job`.`project_id` = 1)) union select 'DONE' AS `St`,count('dossier_id') AS `total` from `job` where ((`job`.`status` = 'DONE') and (`job`.`project_id` = 1)) union select 'RUNNING' AS `St`,count('dossier_id') AS `total` from `job` where ((`job`.`status` = 'RUNNING') and (`job`.`project_id` = 1)) union select 'TOTAL' AS `St`,count('dossier_id') AS `total` from `job` where (`job`.`project_id` = 1) ;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `counter`
--
ALTER TABLE `counter`
  ADD PRIMARY KEY (`project_id`,`counter`);

--
-- Indici per le tabelle `dossier`
--
ALTER TABLE `dossier`
  ADD PRIMARY KEY (`id`,`project_id`),
  ADD KEY `DossierID` (`id`),
  ADD KEY `fk_project_id_project_id` (`project_id`);

--
-- Indici per le tabelle `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `project_id_2` (`project_id`,`dossier_id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indici per le tabelle `material_position`
--
ALTER TABLE `material_position`
  ADD PRIMARY KEY (`dossier_id`,`project_id`,`row_number`);

--
-- Indici per le tabelle `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `job`
--
ALTER TABLE `job`
  MODIFY `id` mediumint(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27875;

--
-- AUTO_INCREMENT per la tabella `project`
--
ALTER TABLE `project`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `counter`
--
ALTER TABLE `counter`
  ADD CONSTRAINT `fk_counter_project_id_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `job`
--
ALTER TABLE `job`
  ADD CONSTRAINT `fk_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`);

--
-- Limiti per la tabella `material_position`
--
ALTER TABLE `material_position`
  ADD CONSTRAINT `fk_dossier_id_project_id_dossier_dossier_id_project_id` FOREIGN KEY (`dossier_id`,`project_id`) REFERENCES `dossier` (`id`, `project_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
