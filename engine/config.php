<?php

$config['PROD'] = array(
	'webserv'	=>	'https://www.wedat.it',
	'host'		=>	'mysqlhost',
	'dbname'	=>	'wedat_db1',
	'user'		=>	'wedat_user1',
	'pass'		=>	'O1EyrwXfP',
	'mcCstLogin'=>	'axainsurance',
	'mcCstNum'	=>	'3132952',
	'mcCstSgn'	=>	'akEwRUF3TUNxTUt5NlFmTTVCdGd5UzYwbEdxcW1qdHpQRGFoSlJneHBvTGhreURMc0VCOG0raHJJd3F3ZVpnWFpMN1dCMTZRUVdSMlRqU2lDemVS',
	'mcIntPtNum'=>	'3132952',
	'mcIntPtSqn'=>	'AA1B25142947B830DD57B58409E479409DA18EADD5866E0DC0AB9D0F8BD0419B'
);

$config['TEST'] = array(
    //'ftpsite'	=>	'hostinglinux18.welcomeitalia.it',
    'ftpsite'	=>	'81.31.147.143',
    'ftpport'	=>	'100',
    'ftpproto'	=>	'sftp',
	'ftpuser'	=>	'webmaster@wedat.it',
	'ftppass'	=>	'wSgy4bBTq',
	'ftproot'   =>  '/public_ftp/photo_capture_dev',
	'localrepo' =>  'download',
	'processed' =>  'processed',
	'mcCstLogin'=>	'ferrsimo',
	'mcCstNum'	=>	'3131411',
	'mcCstSgn'	=>	'akEwRUF3TUN2WmNqeFNYa3R0Rmd5U3BRdW1sU2ZqM0tIc0kvbHE3WUVDZ01SbWFMOTROM2NJQS8yT0dFYVlOY2hOUkpMdmdlNTFoMHhlbz0=',
	'mcIntPtNum'=>	'3131411',
	//'mcIntPtSqn'=>	'jA0EAwMCrjjoCCEuBBBgySvQtBjnp+NKnkteqoMkkzMRZlXddAQsnEG1vdDRAyRfRq+a41mrUtxJrmOg',
	'mcIntPtSqn'=>	'F6A3EAB0734F6E523CB1F05D7B44C8B740BB7B6405F57FB41EAA175EB1AC7332',
    'path'      =>  '/public_ftp/photo_capture_dev',
    'interval'  =>  30,
    'mcHost'    =>  'https://www.dat.de/',
    'mcCtrType' => 'vro_calculation',
    'mcTemplId' => '139960',    //SilverDat PRO 2.15
    'mcCountry' => 'IT',
    'mcLang'    => 'it_IT',
    'mcCurr'    => 'EUR',
    'mcNetwork' => 'ANIA'
);

$config['TESTGOLD'] = array(
	'webserv'	=>	'http://localhost',
	'host'		=>	'localhost',
	'dbname'	=>	'axa_it_stats',
	'user'		=>	'axa_it_stats',
	'pass'		=>	'zV7UB16dzlCDJN59',
	'mcCstLogin'=>	'AxaIT002',
	'mcCstNum'	=>	'3132671',
	'mcCstSgn'	=>	'akEwRUF3TUNZYnpjU0J3WlI1dGd5U3Bjc04zWXJzakEyWjJmV3Nhdk8rcmMrQnIyc1dEWEIwS3Q5c1p5MFhnWlJYb1lES0FaQjBrSVdKRT0',
	'mcIntPtNum'=>	'3132952',
	'mcIntPtSqn'=>	'AA1B25142947B830DD57B58409E479409DA18EADD5866E0DC0AB9D0F8BD0419B'
);

return $config;