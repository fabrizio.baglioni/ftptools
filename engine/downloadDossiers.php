<?php
include_once'AXA_export.php';

$settings = new DownloadDossierSettings();
$urlParams = new UrlParameters($_GET);
$settings->setSettingsProvider($urlParams);

$projectId = $settings->getProjectId();

$env = 'PROD';
$dbc = new DbConnector();
$dbc->connectPDO();

$pageLimit=100;
if (isset($_GET["pageOffset"])) $pageOffset=$_GET["pageOffset"];

if (isset($_GET["pageOffset"])) 
	$pageOffset=$_GET["pageOffset"];
else
	$pageOffset = $dbc->getOffsetToProcess($projectId);

if (defined('STDIN')) 
	if ($argv[1] == '20younger')
		$pageOffset = $pageOffset % 20;

echo "Processing offset $pageOffset";
echo '</br>';

$lc = new ListContract($pageLimit, $pageOffset, $env, $settings);
$contracts = $lc->perform();

if ($contracts == null) //this means that we went over the last dossier; the inbox is over, so we have to start from dossier 1 again
{
	$dbc->startOverAgain($projectId);
	die('Reached last dossier, re-starting from page one');
}

$contracts_added = '';
foreach($contracts as $contract)
{
	//If there is no calculation, we don't need to check the dossier
	if(!isset($contract->totalDamage) || (int)$contract->totalDamage==0) continue;
	echo 'addContractToJobs: ';
	echo '</br>';
	echo '<hr>';
	$dbc->addContractToJobs($projectId, $contract->contractId, $contract->contractModified);
	$contracts_added .= $contract->contractId.',';
}
