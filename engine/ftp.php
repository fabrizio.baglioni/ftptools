<?php

define ('FTP_RETR_MOVE', 'move');
define ('FTP_RETR_COPY', 'copy');
define ('FTP_DIRECTORY_SEPARATOR', '/');

set_include_path(get_include_path() . PATH_SEPARATOR . 'phpseclib');

//include('Net/SSH2.php');

class FtpConnector
{
	protected $_connId;
	protected $_ip;
	protected $_port;
	protected $_user;
	protected $_pass;
	
	public function __construct($ip, $port, $user, $pass)
	{
		$this->_ip   = $ip;
		$this->_port = $port;
		$this->_user = $user;
		$this->_pass = $pass;
	}
	
	public function connect()
	{
		// set up basic connection
		$this->_connId = ftp_connect($this->_ip); 
		
		
		// login with username and password
		$login_result = ftp_login($this->_connId, $this->_user, $this->_pass); 

		ftp_pasv($this->_connId, true);
		
		// check connection
		if ((!$this->_connId) || (!$login_result)) { 
			echo "FTP connection has failed!";
			echo "Attempted to connect to $this->_ip for user $this->_user";
		} else {
			echo "Connected to $this->_ip, for user $this->_user";
		}

	}
	
	public function listDir($directory)
	{
		return ftp_nlist($this->_connId, $directory);
	}
	
	public function retrieve($remote_file, $local_file)
	{
		// open some file to write to
		$handle = fopen($local_file, 'w');
		
		// try to download $remote_file and save it to $handle
		
		$result = ftp_fget($this->_connId, $handle, $remote_file, FTP_ASCII, 0);
		fclose($handle);
		
		if ($result) {
			echo "successfully written to $local_file\n";
			return true;
		} else {
			echo "There was a problem while downloading $remote_file to $local_file\n";
			return false;
		}
	}
	
	public function deleteFile($filename)
	{
		return ftp_delete($this->_connId, $filename);
	}
	
	public function close()
	{
		ftp_close($this->_connId);
	}
	
	public function retrieve_all($directory, $destination, $mode = FTP_RETR_COPY)
	{
		$list = array_diff(($this->listDir($directory)), array("$directory/..", "$directory/."));
		
		if ($list == false) return;
		
		foreach ($list as $fileToDownload)
		{
			$fileName = basename($fileToDownload);
			$copyResult = $this->retrieve($fileToDownload, "$destination/$fileName");
			if ($copyResult)
			{
				$deleteResult = $this->deleteFile($fileToDownload);
				if (!$deleteResult)
				{
					echo "Could not delete file $fileToDownload";
				}
			}
		}
	}
	
	public function upload($uploaddir, $fileName)
	{
		$_FILES['userfile']['name'] = $fileName;
		$_FILES['userfile']['tmp_name'] = "$fileName_tmp";
		
		$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);

		if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
			echo "File is valid, and was successfully uploaded.\n";
		} else {
			echo "Possible file upload attack!\n";
		}
	}
	
}

class CurlSessionActions
{

//$url = 'sftp://' . $sftpServer . ':' . $sftpPort . $sftpRemoteDir . '/' . basename($dataFile);
//$ch = curl_init($url);
}

class SftpConnector extends FtpConnector
{
	private $_sftpConnection;
	private $_curl;

	public function connect()
	{
		$connection = ssh2_connect($this->_ip, $this->_port);
		ssh2_auth_password($connection, $this->_user, $this->_pass);

		$sftp = ssh2_sftp($connection);
		
		$this->_sftpConnection = $connection;

		//$stream = fopen('ssh2.sftp://' . intval($sftp) . '/path/to/file', 'r');
	}

	public function curlInit()
    {
        if ($this->_curl == null)
            $this->_curl = curl_init();
        else
            curl_reset($this->_curl);

        return $this->_curl;
    }

	public function curlClose()
    {
        return curl_close($this->_curl);
    }

	public function listFolderContent($folder)
    {
        //echo 'listFolderContent</br>';
        $url = "sftp://$this->_ip$folder/";
        $curl = $this->curlInit();

        if ($curl === FALSE) die('FAIL1');

        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_PORT,$this->_port);
        curl_setopt($curl, CURLOPT_FTPLISTONLY, 1);
        curl_setopt($curl, CURLOPT_USERPWD, "$this->_user:$this->_pass");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec ($curl);

        if ($result === FALSE)
        {
            echo curl_error($curl);
            //curl_close ($curl);
            die('FAIL2');
        }
        //curl_close ($curl);

        $content = explode("\n",$result);
        $content = array_diff($content, [".", "..", ""]);
        
        return $content;
    }

	public function downloadFile($remote, $local)
    {
        //echo 'downloadFile</br>';
        $path = "sftp://$this->_ip$remote";
        $curl = $this->curlInit();
        $file = fopen($local, 'w');

        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($curl, CURLOPT_URL, $path); #input
        curl_setopt($curl, CURLOPT_PORT,$this->_port);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FILE, $file); #output
        curl_setopt($curl, CURLOPT_USERPWD, "$this->_user:$this->_pass");
        $ret = curl_exec($curl);
        //var_dump(curl_error($curl));
        //curl_close($curl);
        fclose($file);

        return $ret;
    }

	public function deleteFile($remote)
    {
        $path = "sftp://$this->_ip$remote";
        $curl = $this->curlInit();

        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($curl, CURLOPT_URL, "sftp://$this->_ip"); #input
        curl_setopt($curl, CURLOPT_PORT,$this->_port);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_QUOTE, array('rm ' . $remote));
        curl_setopt($curl, CURLOPT_USERPWD, "$this->_user:$this->_pass");
        @curl_exec($curl);  //too much verbose
    }

	public function deleteDir($remote)
    {
        $path = "sftp://$this->_ip$remote";
        $curl = $this->curlInit();

        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($curl, CURLOPT_URL, "sftp://$this->_ip"); #input
        curl_setopt($curl, CURLOPT_PORT,$this->_port);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_QUOTE, array('rmdir ' . $remote));
        curl_setopt($curl, CURLOPT_USERPWD, "$this->_user:$this->_pass");
        @curl_exec($curl);  //too much verbose
    }

	public function upload($uploaddir, $fileName)
	{
		$resFile = fopen("ssh2.sftp://{$this->_sftpConnection}/$uploaddir/".$fileName, 'w');
		$srcFile = fopen("output/".$fileName, 'r');
		$writtenBytes = stream_copy_to_stream($srcFile, $resFile);
		fclose($resFile);
		fclose($srcFile);
	}
	
	public function upload2($uploaddir, $fileName)
	{
		$dataFile      = "output/$fileName";
		$sftpServer    = $this->_ip;
		$sftpUsername  = $this->_user;
		$sftpPassword  = $this->_pass;
		$sftpPort      = $this->_port;
		$sftpRemoteDir = $uploaddir;
		 
		$url = 'sftp://' . $sftpServer . ':' . $sftpPort . $sftpRemoteDir . '/' . basename($dataFile);
		$ch = $this->curlInit($url);
		 		 
		$fh = fopen($dataFile, 'r');
		 
		if ($fh) {
			curl_setopt($ch, CURLOPT_USERPWD, $sftpUsername . ':' . $sftpPassword);
			curl_setopt($ch, CURLOPT_UPLOAD, true);
			curl_setopt($ch, CURLOPT_PROTOCOLS, CURLPROTO_SFTP);
			curl_setopt($ch, CURLOPT_INFILE, $fh);
			curl_setopt($ch, CURLOPT_INFILESIZE, filesize($dataFile));
			curl_setopt($ch, CURLOPT_VERBOSE, true);
		 
			$verbose = fopen('php://temp', 'w+');
			curl_setopt($ch, CURLOPT_STDERR, $verbose);
		 
			$response = curl_exec($ch);
			$error = curl_error($ch);
			//curl_close($ch);
		 
			if ($response) {
				echo "Success";
			} else {
				echo "Failure";
				rewind($verbose);
				$verboseLog = stream_get_contents($verbose);
				echo "Verbose information:\n" . $verboseLog . "\n";
			}
		} else 
		{
			echo "Failureeeeeeee: ".'sftp://' . $sftpServer . ':' . $sftpPort . $sftpRemoteDir . '/' . basename($dataFile);
		}
	}
}