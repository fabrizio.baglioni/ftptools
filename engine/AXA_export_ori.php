<?php
//The AXA credentials to use the web service 
$customerLogin="axainsurance";
$customerNumber="3132952";
$customerSignature="akEwRUF3TUNxTUt5NlFmTTVCdGd5UzYwbEdxcW1qdHpQRGFoSlJneHBvTGhreURMc0VCOG0raHJJd3F3ZVpnWFpMN1dCMTZRUVdSMlRqU2lDemVS";
$interfacePartnerNumber="3132952";
$interfacePartnerSignature="AA1B25142947B830DD57B58409E479409DA18EADD5866E0DC0AB9D0F8BD0419B";
$con=mysqli_connect("mysql1.clarahost.es", "dateu", "dteu", "dateu");

//Create the Web Service client with the credentials
$opts = array( 
			  'http'=>array( 
				'header'=>"customerLogin: ".$customerLogin."\r\n" . 
						  "customerNumber: ".$customerNumber."\r\n" .
						  "customerSignature: ".$customerSignature."\r\n" .
						  "interfacePartnerNumber: ".$interfacePartnerNumber."\r\n" .
						  "interfacePartnerSignature: ".$interfacePartnerSignature."\r\n"
			  ) 
);
$opts=stream_context_create($opts);
$client = new SoapClient("http://noncdn-www.datgroup.com/myClaim/soap/v2/MyClaimExternalService?wsdl", array('stream_context' => $opts, 'exceptions'=>true, 'trace'=>1, 'cache_wsdl' => 'WSDL_CACHE_NONE'));
$examples=10;
$pageOffset=0;
if (isset($_GET["pageOffset"])) $pageOffset=$_GET["pageOffset"];
/*
$pageOffset=8500;
$pageOffset=10600;
$pageOffset=11600;
$pageOffset=2700;
*/

while(true){
	$listContracts="<myc:listContracts xmlns:myc='http://www.dat.eu/myClaim/soap/v2/MyClaimExternalService'>
	<pageLimit>100</pageLimit>
	<pageOffset>".$pageOffset."</pageOffset>
</myc:listContracts>";
	$listContracts=new SoapVar($listContracts, XSD_ANYXML);
	try {
		$listContracts = $client->__soapCall("listContracts", array($listContracts));
	} catch(SoapFault $e){
		break;
	}
	
	//If there are no more contracts, stop the script
	if(!isset($listContracts->return)) break;
	unset($listContractsArray);
	if(count($listContracts->return)>1) $listContractsArray=$listContracts->return;
	else $listContractsArray[]=$listContracts->return;
	foreach($listContractsArray as $contract){
		//echo $contract->contractId."<br/>";
		//If there is no calculation, we don't need to check the dossier
		if(!isset($contract->totalDamage) || (int)$contract->totalDamage==0) continue;
		//Check if we already got this claim info in the DB
		$sql="SELECT id FROM AXA_claims WHERE DossierID=".$contract->contractId;
		$query=mysqli_query($con, $sql);
		if(mysqli_num_rows($query)>0) continue;
		//Calling the getContract method
		$params=array('contractId' => $contract->contractId, 'isWithHistoricalCalculations' => 0);
		
		try{
			$dossier=$client->__soapCall("getContract", array('parameters' => $params));
		} catch(SoapFault $e){
			echo("ERROR: ".$e->faultstring."<br>");
			continue;
		}
		$dossier=$dossier->return;
		
		$paramsHistory=array('contractId' => $contract->contractId);
		try{
			$dossierHistories=$client->__soapCall("listContractHistory", array('parameters' => $paramsHistory));
		} catch(SoapFault $e){
			echo("ERROR: ".$e->faultstring."<br>");
			continue;
		}
		$entryDate='';
		foreach ($dossierHistories->return as $dossierHistory) {
			$statusName = $dossierHistory->statusName;
			if ($statusName == 'Calcolato') {
				$entryDate = $dossierHistory->entryDate;
				continue;
			}
		}

		//If there are no spare parts, skip this dossier
		if(!isset($dossier->Dossier->RepairCalculation->CalcResultCustomerRanking->MaterialPositions->MaterialPosition)) continue;
		$DossierID=$dossier->Dossier->DossierId->_;
		$Assessment_Creation_Date=$dossier->Dossier->CreateDate->_;
		$Assessment_Last_Update_Date=$dossier->Dossier->ChangeDate->_;
		$Status=$dossier->statusName;
		$VIN=$dossier->Dossier->Vehicle->VehicleIdentNumber->_;
		$Ecode=$dossier->Dossier->Vehicle->DatECode->_;
		$ConstructionTime=$dossier->Dossier->Vehicle->ConstructionTime->_;
		$Number_Plate="";
		$Opponent_Number_Plate="";
		$assignmentID="";
		$DamageNumber="";
		$AspisAssessmentNumber="";
		foreach($dossier->customTemplateData->entry as $custom){
			if($custom->key=="assignmentID"){
				$assignmentID=$custom->value;
			}
			if($custom->key=="DamageNumber"){
				$DamageNumber=$custom->value;
			}
			if($custom->key=="AspisAssessmentNumber"){
				$AspisAssessmentNumber=$custom->value;
			}
			if($custom->key=="LicenseNumber"){
				$Number_Plate=$custom->value;
			}
			if($custom->key=="Opponent_LicenseNumber"){
				$Opponent_Number_Plate=$custom->value;
			}
			if($assignmentID!="" && $DamageNumber!="" && $AspisAssessmentNumber!="" && $Number_Plate!="" && $Opponent_Number_Plate!="") break;
		}
		unset($materialPositionArray);
		if(count($dossier->Dossier->RepairCalculation->CalcResultCustomerRanking->MaterialPositions->MaterialPosition)>1) $materialPositionArray=$dossier->Dossier->RepairCalculation->CalcResultCustomerRanking->MaterialPositions->MaterialPosition;
		else $materialPositionArray[]=$dossier->Dossier->RepairCalculation->CalcResultCustomerRanking->MaterialPositions->MaterialPosition;
		foreach($materialPositionArray as $materialPosition){
			$DVN=$materialPosition->DATProcessId->_;
			$Reference_Spare_Part=$materialPosition->PartNumber->_;
			$Description=mysqli_real_escape_string($con, $materialPosition->Description->_);
			$Spare_Part_Price=$materialPosition->ValueTotalCorrected->_;
			//Insert in the DB
			$sql="INSERT INTO AXA_claims(`DossierID`, `assignmentID`, `DamageNumber`,`AspisAssessmentNumber`, `Last_Calculation_Date`,`Assessment_Creation_Date`, `Assessment_Last_Update_Date`, `Status`, `VIN`, `Number_Plate`, `Opponent_Number_Plate`, `Ecode`, `ConstructionTime`, `DVN`, `Reference_Spare_Part`, `Description`, `Spare_Part_Price`) ";
			$sql.="VALUES ($DossierID, '$assignmentID', '$DamageNumber', '$AspisAssessmentNumber', '$entryDate', '$Assessment_Creation_Date', '$Assessment_Last_Update_Date', '$Status', '$VIN', '$Number_Plate', '$Opponent_Number_Plate', '$Ecode', '$ConstructionTime', '$DVN', '$Reference_Spare_Part', '$Description', '$Spare_Part_Price')";
			if(!mysqli_query($con, $sql)){
				echo mysqli_error($con) ." ( ".$sql." )<br>";
			}
		}
		//For an example
		/*
		$examples-=1;
		if($examples==0) exit;
		*/
	}
	$pageOffset+=100;
	echo "PageOffset : ".$pageOffset."<br>";
}
echo "PROCESS COMPLETED";
?>