<?php
include_once 'vendor/autoload.php';
include_once 'engine/configurator.php';
include_once 'engine/ftp.php';
include_once 'MyClaimUploader.php';
include_once 'FtpDownloader.php';

set_time_limit (180);

$cut = FALSE;
if (isset($_GET['dvr'])) $cut = $_GET['dvr'];

$start = microtime(true);

$fd = new FtpDownloader(getenv('ftpsite'), getenv('ftpport'), getenv('ftpuser'), getenv('ftppass'));

$fd->getMappedFolders($cut);

if (!$fd->scanFtpSite()) die($fd->getLastError());

if (!$fd->downloadScannedTree()) die($fd->getLastError());

$cl = New ClaimLister();
$cl->buildFromRangesArray($fd->getRanges());

$mcu = new MyClaimUploader();
$mcu->processClaimList($cl);

echo '<pre>';
$time_elapsed_secs = microtime(true) - $start;
echo "EXECUTION TOOK $time_elapsed_secs seconds";
print_r($fd->getPaths());
print_r($fd->getRanges());
echo '</pre>';
